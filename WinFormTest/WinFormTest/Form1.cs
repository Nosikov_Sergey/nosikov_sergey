﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace WinFormTest
{
    public partial class Form1 : Form
    {
        Timer timer = new Timer();
        private Data data = new Data();
        public Form1()
        {
            InitializeComponent();
            data.Open();
            Puth.Text = data.RootDirectory;
            NameFile.Text = data.FileName;
            CheckFileName.Checked = data.CheckFileName;
            CheckTextInFile.Checked = data.CheckTextInFile;
            TextInFile.Text = data.TextInFile;
        }

        private void SelectDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (DialogResult.OK == folderBrowserDialog.ShowDialog())
            {
                Puth.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void CheckFileName_CheckedChanged(object sender, EventArgs e)
        {
            NameFile.Enabled = ((CheckBox)sender).Checked;
        }

        private void CheckTextInFile_CheckedChanged(object sender, EventArgs e)
        {
            TextInFile.Enabled = ((CheckBox)sender).Checked;
        }

        private async void Search_Click(object sender, EventArgs e)
        {
            SelectDirectory.Enabled = false;
            NameFile.Enabled = false;
            CheckFileName.Enabled = false;
            CheckTextInFile.Enabled = false;
            TextInFile.Enabled = false;
            Search.Enabled = false;
            Pause.Enabled = true;
            Stop.Enabled = true;
            CoutnFiles = -1;
            Tree.Nodes.Clear();
            sec = 0;

            timer.Tick += new EventHandler((Obj,EvAr)=> {
                sec++;
                setTime(String.Format("Время: {0}:{1:00}:{2:00}", sec / 3600, sec / 60 % 60, sec % 60));
            }) ;

            timer.Interval = 1000;
            timer.Start();
            DirectoryInfo root = new DirectoryInfo(Puth.Text);
            if (root.Exists)
            {
                TreeNode RootNode = new TreeNode(root.Name);
                Tree.Nodes.Add(RootNode);
                QueueDirectories.Enqueue(root);
                Stoped = false;
                await Task.Run(() =>
                {
                    StartSearch();
                });
            }
            else
            {
                MessageBox.Show("Не выбран каталог");
                StopCode();
            }
        }

        public Queue<DirectoryInfo> QueueDirectories = new Queue<DirectoryInfo>();
        public Queue<FileInfo> Queuefiles = new Queue<FileInfo>();

        private int CoutnFiles = 0;
        


        private int sec;

        private void setInfoFile(string text)
        {
            Tool.BeginInvoke(new Action(() => InfoText.Text = text));
        }
        private void setInfoCountFiles()
        {
            CoutnFiles++;
            Tool.BeginInvoke(new Action(() => FileCount.Text = $"Кол-во обработанных файлов: {CoutnFiles}"));
        }

        private void setTime(string text)
        {
            Tool.BeginInvoke(new Action(() => time.Text = text));
        }

        private void StartSearch()
        {
            while (!Stoped)
            {
                if (Queuefiles.Count != 0)
                {
                    //Читаем файлы
                    FileInfo CurrentFile = Queuefiles.Dequeue();

                    setInfoFile(CurrentFile.FullName);
                    if (CheckFileName.Checked && (CurrentFile.Name).Contains(NameFile.Text))
                    {
                        Insert(CurrentFile);
                    }
                    else if (CheckTextInFile.Checked)
                    {
                        bool buf = false;
                        try { buf = File.ReadAllText(CurrentFile.FullName).Contains(TextInFile.Text); } catch { }
                        if (buf) Insert(CurrentFile);
                    }
                    setInfoCountFiles();
                }
                else if (QueueDirectories.Count != 0)
                {
                    DirectoryInfo CurrentDirectory = QueueDirectories.Dequeue();
                    try
                    {

                        foreach (FileInfo fileItem in CurrentDirectory.GetFiles())
                        {
                            Queuefiles.Enqueue(fileItem);
                        }
                    }
                    catch { }
                    try
                    {
                        foreach (DirectoryInfo directoryItem in CurrentDirectory.GetDirectories())
                        {
                            QueueDirectories.Enqueue(directoryItem);
                        }
                    }
                    catch { }
                }
                else
                {
                    setInfoFile("Поиск окончен");
                    this.BeginInvoke(new Action(() => StopCode()));
                    break;
                }

            }
        }

        private void Insert(FileInfo file)
        {
            string[] buf = file.FullName.TrimStart(Puth.Text.ToArray()).Split('\\');

            Tree.BeginInvoke(new Action(() =>
            {
                TreeNode CurrentNode = Tree.Nodes[0];
                for (int i = 0; i < buf.Length; i++)
                {
                    TreeNode FindNode = null;
                    foreach (TreeNode item in CurrentNode.Nodes)
                    {
                        if (item.Text == buf[i])
                        {
                            FindNode = item;
                            break;
                        }
                    }

                    if (FindNode != null)
                    {
                        CurrentNode = FindNode;
                    }
                    else
                    {
                        TreeNode bufNode = new TreeNode(buf[i]);
                        CurrentNode.Nodes.Add(bufNode);
                        CurrentNode = bufNode;
                    }
                }

            }));



        }

        bool Stoped;

        private void Pause_Click(object sender, EventArgs e)
        {
            Pause.Text = Stoped ? "Пауза" : "Продолжить";
            Stoped = !Stoped;
            if (!Stoped) Task.Run(() =>
            {
                timer.Start();
                StartSearch();
            });
            timer.Stop();
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            Stoped = true;
            StopCode();
            setInfoFile("Остановленно");
            timer.Stop();
        }

        private void StopCode()
        {
            SelectDirectory.Enabled = true;
            NameFile.Enabled = true;
            CheckFileName.Enabled = true;
            CheckTextInFile.Enabled = true;
            TextInFile.Enabled = true;
            Search.Enabled = true;
            Pause.Enabled = false;
            Stop.Enabled = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stoped = true;
            data.RootDirectory = Puth.Text;
            data.FileName = NameFile.Text;
            data.CheckFileName = CheckFileName.Checked;
            data.CheckTextInFile = CheckTextInFile.Checked;
            data.TextInFile = TextInFile.Text;
            data.Save();
        }
        
    }
}
