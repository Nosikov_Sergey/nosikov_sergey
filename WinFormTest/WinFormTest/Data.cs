﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WinFormTest
{
    [Serializable]
    public class Data
    {
        XmlSerializer formatter = new XmlSerializer(typeof(Data));

        public void Open()
        {
            if (File.Exists("ProgData.xml"))
            {
                using (FileStream stream = new FileStream("ProgData.xml", FileMode.Open))
                {
                    Data newData = (Data)formatter.Deserialize(stream);
                    RootDirectory = newData.RootDirectory;
                    FileName = newData.FileName;
                    TextInFile = newData.TextInFile;
                    CheckFileName = newData.CheckFileName;
                    CheckTextInFile = newData.CheckTextInFile;
                }
            }
        }

        public string RootDirectory { get; set; } = "None";
        public string FileName { get; set; } = "";
        public string TextInFile { get; set; } = "";
        public bool CheckFileName { get; set; } = false;
        public bool CheckTextInFile { get; set; } = false;

        public void Save()
        {
            using (FileStream stream = new FileStream("ProgData.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(stream, this);
            }
        }
    }
}
