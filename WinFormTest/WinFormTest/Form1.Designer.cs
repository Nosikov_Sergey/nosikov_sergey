﻿namespace WinFormTest
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tree = new System.Windows.Forms.TreeView();
            this.SelectDirectory = new System.Windows.Forms.Button();
            this.Puth = new System.Windows.Forms.Label();
            this.NameFile = new System.Windows.Forms.TextBox();
            this.CheckFileName = new System.Windows.Forms.CheckBox();
            this.CheckTextInFile = new System.Windows.Forms.CheckBox();
            this.TextInFile = new System.Windows.Forms.TextBox();
            this.Search = new System.Windows.Forms.Button();
            this.Stop = new System.Windows.Forms.Button();
            this.Pause = new System.Windows.Forms.Button();
            this.Tool = new System.Windows.Forms.StatusStrip();
            this.time = new System.Windows.Forms.ToolStripStatusLabel();
            this.FileCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.InfoText = new System.Windows.Forms.ToolStripStatusLabel();
            this.Tool.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tree
            // 
            this.Tree.Location = new System.Drawing.Point(12, 12);
            this.Tree.Name = "Tree";
            this.Tree.Size = new System.Drawing.Size(413, 495);
            this.Tree.TabIndex = 0;
            // 
            // SelectDirectory
            // 
            this.SelectDirectory.Location = new System.Drawing.Point(454, 12);
            this.SelectDirectory.Name = "SelectDirectory";
            this.SelectDirectory.Size = new System.Drawing.Size(201, 23);
            this.SelectDirectory.TabIndex = 1;
            this.SelectDirectory.Text = "Указать стартовую директорию";
            this.SelectDirectory.UseVisualStyleBackColor = true;
            this.SelectDirectory.Click += new System.EventHandler(this.SelectDirectory_Click);
            // 
            // Puth
            // 
            this.Puth.AutoSize = true;
            this.Puth.Location = new System.Drawing.Point(451, 38);
            this.Puth.Name = "Puth";
            this.Puth.Size = new System.Drawing.Size(27, 13);
            this.Puth.TabIndex = 3;
            this.Puth.Text = "Non";
            // 
            // NameFile
            // 
            this.NameFile.Enabled = false;
            this.NameFile.Location = new System.Drawing.Point(595, 63);
            this.NameFile.Name = "NameFile";
            this.NameFile.Size = new System.Drawing.Size(193, 20);
            this.NameFile.TabIndex = 4;
            // 
            // CheckFileName
            // 
            this.CheckFileName.AutoSize = true;
            this.CheckFileName.Location = new System.Drawing.Point(454, 66);
            this.CheckFileName.Name = "CheckFileName";
            this.CheckFileName.Size = new System.Drawing.Size(135, 17);
            this.CheckFileName.TabIndex = 6;
            this.CheckFileName.Text = "Шаблон имени файла";
            this.CheckFileName.UseVisualStyleBackColor = true;
            this.CheckFileName.CheckedChanged += new System.EventHandler(this.CheckFileName_CheckedChanged);
            // 
            // CheckTextInFile
            // 
            this.CheckTextInFile.AutoSize = true;
            this.CheckTextInFile.Location = new System.Drawing.Point(454, 100);
            this.CheckTextInFile.Name = "CheckTextInFile";
            this.CheckTextInFile.Size = new System.Drawing.Size(183, 17);
            this.CheckTextInFile.TabIndex = 7;
            this.CheckTextInFile.Text = "Текст, содержащийся в файле";
            this.CheckTextInFile.UseVisualStyleBackColor = true;
            this.CheckTextInFile.CheckedChanged += new System.EventHandler(this.CheckTextInFile_CheckedChanged);
            // 
            // TextInFile
            // 
            this.TextInFile.Enabled = false;
            this.TextInFile.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TextInFile.Location = new System.Drawing.Point(454, 123);
            this.TextInFile.Multiline = true;
            this.TextInFile.Name = "TextInFile";
            this.TextInFile.Size = new System.Drawing.Size(334, 355);
            this.TextInFile.TabIndex = 8;
            // 
            // Search
            // 
            this.Search.Location = new System.Drawing.Point(454, 484);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(96, 23);
            this.Search.TabIndex = 9;
            this.Search.Text = "Поиск";
            this.Search.UseVisualStyleBackColor = true;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // Stop
            // 
            this.Stop.Enabled = false;
            this.Stop.Location = new System.Drawing.Point(687, 484);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(101, 23);
            this.Stop.TabIndex = 10;
            this.Stop.Text = "Стоп";
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.Stop_Click);
            // 
            // Pause
            // 
            this.Pause.Enabled = false;
            this.Pause.Location = new System.Drawing.Point(567, 484);
            this.Pause.Name = "Pause";
            this.Pause.Size = new System.Drawing.Size(101, 23);
            this.Pause.TabIndex = 11;
            this.Pause.Text = "Пауза";
            this.Pause.UseVisualStyleBackColor = true;
            this.Pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // Tool
            // 
            this.Tool.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.time,
            this.FileCount,
            this.InfoText});
            this.Tool.Location = new System.Drawing.Point(0, 510);
            this.Tool.Name = "Tool";
            this.Tool.Size = new System.Drawing.Size(800, 22);
            this.Tool.TabIndex = 13;
            this.Tool.Text = "statusStrip1";
            // 
            // time
            // 
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(45, 17);
            this.time.Text = "Время:";
            // 
            // FileCount
            // 
            this.FileCount.Name = "FileCount";
            this.FileCount.Size = new System.Drawing.Size(177, 17);
            this.FileCount.Text = "Кол-во обработанных файлов:";
            // 
            // InfoText
            // 
            this.InfoText.Name = "InfoText";
            this.InfoText.Size = new System.Drawing.Size(64, 17);
            this.InfoText.Text = "Ожидание";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 532);
            this.Controls.Add(this.Tool);
            this.Controls.Add(this.Pause);
            this.Controls.Add(this.Stop);
            this.Controls.Add(this.Search);
            this.Controls.Add(this.TextInFile);
            this.Controls.Add(this.CheckTextInFile);
            this.Controls.Add(this.CheckFileName);
            this.Controls.Add(this.NameFile);
            this.Controls.Add(this.Puth);
            this.Controls.Add(this.SelectDirectory);
            this.Controls.Add(this.Tree);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Tool.ResumeLayout(false);
            this.Tool.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView Tree;
        private System.Windows.Forms.Button SelectDirectory;
        private System.Windows.Forms.Label Puth;
        private System.Windows.Forms.TextBox NameFile;
        private System.Windows.Forms.CheckBox CheckFileName;
        private System.Windows.Forms.CheckBox CheckTextInFile;
        private System.Windows.Forms.TextBox TextInFile;
        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.Button Stop;
        private System.Windows.Forms.Button Pause;
        private System.Windows.Forms.StatusStrip Tool;
        private System.Windows.Forms.ToolStripStatusLabel InfoText;
        private System.Windows.Forms.ToolStripStatusLabel FileCount;
        private System.Windows.Forms.ToolStripStatusLabel time;
    }
}

